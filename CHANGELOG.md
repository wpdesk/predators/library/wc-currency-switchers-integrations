## [1.4.0] - 2024-08-02
### Changed
- psr/log version requirement

## [1.3.0] - 2024-05-21
### Removed
- CURCY filters. the CURCY plugin provides its own compatibility with flexible shipping

## [1.2.0] - 2023-04-04
### Added
- Support for CURCY – Multi Currency for WooCommerce (https://wordpress.org/plugins/woo-multi-currency/)

## [1.1.2] - 2023-04-01
### Fixed
- WCML integration

## [1.1.1] - 2023-03-29
### Fixed
- class name faker for php scoper

## [1.1.0] - 2023-03-29
### Added
- currency switchers filters 

## [1.0.2] - 2022-05-24
### Changed
- removed plugin flow library
- added wp builder library

## [1.0.1] - 2020-09-28
### Changed
- allowed wp-plugin-flow 3

## [1.0.0] - 2020-04-28
### Added
- first stable version
