[![Latest Stable Version](https://poser.pugx.org/wpdesk/wc-currency-switchers-integrations/v/stable)](https://packagist.org/packages/wpdesk/wc-currency-switchers-integrations) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wc-currency-switchers-integrations/downloads)](https://packagist.org/packages/wpdesk/wc-currency-switchers-integrations) 
[![License](https://poser.pugx.org/wpdesk/wc-currency-switchers-integrations/license)](https://packagist.org/packages/wpdesk/wc-currency-switchers-integrations)

# Currency Switchers Integrations

A WordPress library containing interfaces, abstracts and basic implementations to be used for currency switchers plugins and integrations.

## Supported switchers
- https://aelia.co/shop/currency-switcher-woocommerce/
- https://wordpress.org/plugins/woo-multi-currency/
- https://wordpress.org/plugins/currency-switcher-woocommerce/
- https://wordpress.org/plugins/woocommerce-currency-switcher/
- https://wordpress.org/plugins/woocommerce-multilingual/
- https://woocommerce.com/products/multi-currency/

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-currency-switchers-integrations
```

## Example usage

Integrating the shipping method ( flexible_shipping ) with [WooCommerce Multicurrency](https://woocommerce.com/products/multi-currency/) plugin:

```php
new \WPDesk\WooCommerce\CurrencySwitchers\ShippingIntegrations( 'flexible_shipping' )
```

## Prefixer

In scoper.inc.php file add `WOOMC` to the whitelist section:

```$php
    'whitelist' => [
    	'\WOOMC\*'
    ],
```
